Step 1: Setup the FQDN

As required by Zammad, you need to properly setup the FQDN on your server instance before you can remotely access the Zammad site.

Use the vi text editor to open the /etc/hosts file.

vi /etc/hosts

Insert the following line before any existing lines.

203.222.xxx.xxx ticket.example.com Ticket

Save and quit.

:wq!

Use the hostname -f command to confirm the result, which will look like this.

ticket.example.com

Step 2: Install OpenJDK 8 packages latest stable release of OpenJDK 8.

apt install -y openjdk-8-jdk

java -version

The output will look like this.

openjdk version "1.8.0_122"
OpenJDK Runtime Environment (build 1.8.0_131-8u131-b11-2ubuntu1.16.04.3-b11)
OpenJDK 64-Bit Server VM (build 25.131-b11, mixed mode)

Finally, setup the JAVA_HOME environment variable.

echo "JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:bin/java::")" | sudo tee -a /etc/profile
source /etc/profile

Step 3: Install Elasticsearch 5 and the mapper-attachments plugin

Zammad uses Elasticsearch to provide the search function. On Ubuntu 16.04, you can install the latest stable release of Elasticsearch 5 using the official Elasticsearch DEB repo as below.

First, download and install the Elasticsearch 5 public signing key.

apt install -y wget
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -

Then, install the apt-transport-https package.

apt install -y apt-transport-https

Save the repository definition to /etc/apt/sources.list.d/elastic-5.x.list.

echo "deb https://artifacts.elastic.co/packages/5.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-5.x.list

Next, install Elasticsearch 5.x using APT.

apt update -y
apt install -y elasticsearch

Finally, start the Elasticsearch service and make it automatically start on boot time.

systemctl daemon-reload
systemctl start elasticsearch.service
systemctl enable elasticsearch.service

In addition, in order to allow Elasticsearch to index file attachments, you need to install the Elasticsearch mapper-attachments plugin as follows.

/usr/share/elasticsearch/bin/elasticsearch-plugin install mapper-attachments

When asked to approve additional permissions, type "Y" and then press "Enter" to finish the installation.

Restart the Elasticsearch service to load the new plugin.

systemctl restart elasticsearch.service

Step 4: Install Zammad 2.0

In order to facilitate the installation process, you can use the official Zammad DEB repo to install Zammad along with various dependencies, including Nginx, PostgreSQL and more, all at the same time.

First of all, make sure that the server instance is using a UTF-8 locale, otherwise PostgreSQL cannot be installed.

locale

If you find any locale settings other than en_US.UTF-8, then you need to switch to that locale as follows.

apt install -y locales
locale-gen en_US.UTF-8
echo "LANG=en_US.UTF-8" | sudo tee /etc/default/locale

After ensuring the locale is correct, download the official Zammad DEB repo.

wget -qO- https://dl.packager.io/srv/zammad/zammad/key | sudo apt-key add -
wget -O /etc/apt/sources.list.d/zammad.list https://dl.packager.io/srv/zammad/zammad/stable/installer/ubuntu/16.04.repo

Install Zammad and all of the required dependencies using APT.

apt update -y
apt install -y zammad

Having Zammad successfully installed, the Zammad service will automatically get started. You can use the following command to confirm the Zammad service's status.

systemctl status zammad.service

Its status will be active (running).

Next, use the following commands to make Zammad work with Elasticsearch and rebuild the index.

zammad run rails r "Setting.set('es_url', 'http://localhost:9200')"
zammad run rake searchindex:rebuild

Add extra Elasticsearch index name space (optional).

zammad run rails r "Setting.set('es_index', Socket.gethostname + '_zammad')"

Ignore common binary file types (optional).

zammad run rails r "Setting.set('es_attachment_ignore', [ '.png', '.jpg', '.jpeg', '.mpeg', '.mpg', '.mov', '.bin', '.exe', '.box', '.mbox' ] )"

Set the max attachment size (optional).

zammad run rails r "Setting.set('es_attachment_max_size_in_mb', 50)"

In order to allow users to remotely access Zammad, you need to modify the Zammad Nginx configuration file.

vi /etc/nginx/sites-available/zammad.conf

Find the line.

 server_name localhost;

Replace localhost with the FQDN of your server instance.

server_name ticket.example.com;

Save and quit.

:wq!

Finally, restart the Nginx service to apply your modifications.

systemctl restart nginx.service

Step 5: Setup SELinux and firewall rules

Use the following commands to determine the status of SELinux on your server instance.

apt install -y policycoreutils

sestatus

If SELinux is enabled in the enforcing mode on your machine, you need to modify a SELinux rule as follows.

apt install -y selinux selinux-basics selinux-utils selinux-policy-ubuntu setools
setsebool httpd_can_network_connect on -P

If SELinux is disabled or enabled in the permissive mode, ignore above modifications and move on.

You also need to modify UFW firewall rules as follows.

ufw app list
ufw allow OpenSSH
ufw allow in "Nginx Full"
ufw enable

Step 6: Setup the new Zammad system from the Web interface

Point your favorite web browser to http://ticket.example.com, and then you will be brought into the Zammad getting started page.

Click the Setup new system button to move on.

In the Administrator Account window, input the administrator's first name, last name, email, and password (twice), and then click the Create button.

In the Organization window, input the organization name and the system URL http://helpdesk.example.com , upload your logo (if any), and then click the Next button.

In the Email Notification window, choose a proper email sending method, and then click the Continue button.

In the Connect Channels window, click the Skip button to finish the initial setup. All channels can be customized later.

That's all for setting up an operational Zammad ticketing system. Feel free to explore the Zammad interface and invite customer service representatives. Thanks for reading.